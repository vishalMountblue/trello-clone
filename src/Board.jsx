import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import "./Board.css";
import { Link } from "react-router-dom";
export const Board = (props) => {
  const data = props.boardData;
  const bull = (
    <Box
      component="span"
      sx={{ display: "inline-block", mx: "2px", transform: "scale(0.8)" }}
    ></Box>
  );
  return (
    <>
      <Link to={`/boards/${data.id}`} >
        <Card
          className="board-card"
          sx={{
            width: 300,
            height: 150,
            backgroundColor: "blue",
            color: "white",
            backgroundImage: `url(${data.prefs.backgroundImage})`,
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        >
          <CardContent>
            <Typography id="board-text" variant="h5" component="div">
              {data.name}
            </Typography>
          </CardContent>
        </Card>
      </Link>
    </>
  );
};
