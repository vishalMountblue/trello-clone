import axios from "axios";
export const fetchBoards = async () => {
  try {
    let boardResponse = await axios.get(
      `https://api.trello.com/1/members/me/boards?key=${
        import.meta.env.VITE_TRELLO_API_KEY
      }&token=${import.meta.env.VITE_TRELLO_TOKEN}`
    );
    return boardResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const generateBoard = async (boardName) => {
  try {
    let createBoardResponse = await axios.post(
      `https://api.trello.com/1/boards/?name=${boardName}&key=${
        import.meta.env.VITE_TRELLO_API_KEY
      }&token=${import.meta.env.VITE_TRELLO_TOKEN}`
    );
    console.log("createBoardResponse",createBoardResponse)
    return createBoardResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const getAllLists = async (board) => {
  try {
    let listResponse = await axios.get(
      `https://api.trello.com/1/boards/${board.id}/lists?key=${
        import.meta.env.VITE_TRELLO_API_KEY
      }&token=${import.meta.env.VITE_TRELLO_TOKEN}`
    );
    return listResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const getAllCards = async (list) => {
  try {
    let cardResponse = await axios.get(
      `https://api.trello.com/1/lists/${list.id}/cards?key=${
        import.meta.env.VITE_TRELLO_API_KEY
      }&token=${import.meta.env.VITE_TRELLO_TOKEN}`
    );
    return cardResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const archive = async (listId) => {
  try {
    let archivedResponse = await axios.put(
      `https://api.trello.com/1/lists/${listId}/closed?key=${
        import.meta.env.VITE_TRELLO_API_KEY
      }&token=${import.meta.env.VITE_TRELLO_TOKEN}`,
      {
        value: "true",
      }
    );
    return archivedResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const deleteCard = async (card) => {
  try {
    let deleteResponse = await axios.delete(
      `https://api.trello.com/1/cards/${card.id}?key=${
        import.meta.env.VITE_TRELLO_API_KEY
      }&token=${import.meta.env.VITE_TRELLO_TOKEN}`
    );
    return deleteResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const getAllCheckList = async (card) => {
  try {
    let checkListResponse = await axios.get(
      `https://api.trello.com/1/cards/${card.id}/checklists?key=${
        import.meta.env.VITE_TRELLO_API_KEY
      }&token=${import.meta.env.VITE_TRELLO_TOKEN}`
    );
    return checkListResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const deleteChecklist = async (checklist) => {
  try {
    let deleteCheckListResponse = await axios.delete(
      `https://api.trello.com/1/checklists/${checklist.id}?key=${
        import.meta.env.VITE_TRELLO_API_KEY
      }&token=${import.meta.env.VITE_TRELLO_TOKEN}`
    );
    return deleteCheckListResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const toggleCheckItem = async (card, checkedValue, checkItem) => {
  try {
    let checkResponse = await axios.put(
      `https://api.trello.com/1/cards/${card.id}/checkItem/${
        checkItem.id
      }?key=${import.meta.env.VITE_TRELLO_API_KEY}&token=${
        import.meta.env.VITE_TRELLO_TOKEN
      }`,
      {
        state: checkedValue,
      }
    );
    return checkResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const discardCheckItem = async (checkItem, checklist) => {
  try {
    let discardedCheckItemResponse = await axios.delete(
      `https://api.trello.com/1/checklists/${checklist.id}/checkItems/${
        checkItem.id
      }?key=${import.meta.env.VITE_TRELLO_API_KEY}&token=${
        import.meta.env.VITE_TRELLO_TOKEN
      }`
    );
    return discardedCheckItemResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const generateList = async (boardId, listName) => {
  try {
    let createListResponse = await axios.post(
      `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${
        import.meta.env.VITE_TRELLO_API_KEY
      }&token=${import.meta.env.VITE_TRELLO_TOKEN}`
    );
    return createListResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const generateCardInList = async (listId, itemName) => {
  try {
    let createCardResponse = await axios.post(
      `https://api.trello.com/1/cards?idList=${listId}&key=${
        import.meta.env.VITE_TRELLO_API_KEY
      }&token=${import.meta.env.VITE_TRELLO_TOKEN}`,
      {
        name: itemName,
      }
    );
    return createCardResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const generateCheckList = async (card, checkListName) => {
  try {
    let createdChecklistResponse = await axios.post(
      `https://api.trello.com/1/cards/${card.id}/checklists?key=${
        import.meta.env.VITE_TRELLO_API_KEY
      }&token=${import.meta.env.VITE_TRELLO_TOKEN}`,
      {
        name: checkListName,
      }
    );
    return createdChecklistResponse;
  } catch (err) {
    return { error: err.message };
  }
};

export const generateCheckItem = async (checklist, checkItem) => {
  try {
    let createCheckItemResponse = await axios.post(
      `https://api.trello.com/1/checklists/${
        checklist.id
      }/checkItems?name=${checkItem}&key=${
        import.meta.env.VITE_TRELLO_API_KEY
      }&token=${import.meta.env.VITE_TRELLO_TOKEN}`
    );

    return createCheckItemResponse;
  } catch (err) {
    return { error: err.message };
  }
};
