import { useState } from "react";
import { Home } from "./Home";
import "./App.css";
import { Routes, Route, Link } from "react-router-dom";
import { Workspace } from "./Workspace";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/boards" element={<Home />}></Route>

        <Route path="/boards/:id" element={<Workspace />}></Route>
      </Routes>
    </>
  );
}

export default App;
