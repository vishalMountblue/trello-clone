import List from "@mui/material/List";
import IconButton from "@mui/material/IconButton";
import Grid from "@mui/material/Grid";
import { useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import CloseIcon from "@mui/icons-material/Close";
import { generateList } from "./Api";
import { Container } from "@mui/material";
import Alert from "@mui/material/Alert";
import Snackbar from '@mui/material/Snackbar';

export const CreateList = (props) => {
  const [isCollapsed, setIsCollapsed] = useState(true);
  const [listName, setListName] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  let boardId = props.board;

  const updateCollapseValue = () => {
    isCollapsed ? setIsCollapsed(false) : setIsCollapsed(true);
  };

  const handleInputChange = (event) => {
    setListName(event.target.value);
  };

  const addList = async () => {
    if (!listName) return;

    let response = await generateList(boardId, listName);

    if (response.error) {
      setLoading(false);
      setError(response.error);
    } else {
      props.listUpdate(response.data);
      setLoading(false);
      setError(null);
    }
  };
  return (
    <>
      <List
        sx={{
          minWidth: 300,
          height: "fit-content",
          bgcolor: "gray",
          mx: 3,
          px: 1,
        }}
      >
        {isCollapsed ? (
          <Card onClick={updateCollapseValue} sx={{ width: "100%", my: 2 }}>
            <CardActions>
              <Button size="small">Add Another List</Button>
            </CardActions>
          </Card>
        ) : (
          <Grid container sx={{ flexDirection: "column", width: "100%" }}>
            <Grid item>
              <Card sx={{ width: "100%" }}>
                <TextField
                  sx={{ width: "100%" }}
                  id="outlined-multiline-static"
                  rows={4}
                  placeholder="Enter your list name...."
                  onChange={handleInputChange}
                />
              </Card>
            </Grid>

            <Grid item>
              <Button onClick={addList} variant="contained" size="small">
                Add List
              </Button>
              <IconButton onClick={updateCollapseValue} aria-label="more">
                <CloseIcon />
              </IconButton>
            </Grid>

            <Container sx={{ my: 1 }}>
              {error && (
                <Snackbar open={true}>
                  <Alert severity="error">{error}</Alert>
                </Snackbar>
              )}
            </Container>
          </Grid>
        )}
      </List>
    </>
  );
};
