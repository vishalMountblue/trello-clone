import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { Button, Container } from "@mui/material";
import { useState } from "react";
import { toggleCheckItem } from "./Api";
import { discardCheckItem } from "./Api";

export const CheckItem = (props) => {
  let checkItem = props.checkItemData;
  let checklist = props.checklistData;
  let card = props.card;
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const COMPLETE = "complete";
  const INCOMPLETE = "incomplete";
  let defaultCheckValue = null;

  checkItem.state === COMPLETE
    ? (defaultCheckValue = true)
    : (defaultCheckValue = false);

  const [checked, setChecked] = useState(defaultCheckValue);
  let checkedValue;

  const handleChange = async (event) => {
    checked ? (checkedValue = INCOMPLETE) : (checkedValue = COMPLETE);
    setChecked(event.target.checked);
    let response = await toggleCheckItem(card, checkedValue, checkItem);
    if (response.error) {
      setLoading(false);
      setError(response.error);
    } else {
      props.modifyCheckItems(response.data);
      setLoading(false);
      setError(null);
    }
  };

  const deleteCheckItem = async () => {
    let response = await discardCheckItem(checkItem, checklist);
    if (response.error) {
      setLoading(false);
      setError(response.error);
    } else {
      props.removeCheckItem(checkItem.id);
      setLoading(false);
      setError(null);
    }
  };

  if (error) {
    return <h3>{error} </h3>;
  }

  return (
    <>
      <Container sx={{ display: "flex", justifyContent: "space-between" }}>
        <FormControlLabel
          control={
            <Checkbox
              checked={checked}
              onChange={handleChange}
              inputProps={{ "aria-label": "controlled" }}
            />
          }
          label={checkItem.name}
        />
        <Button sx={{ my: 1 }} variant="outlined" onClick={deleteCheckItem}>
          Delete
        </Button>
      </Container>
    </>
  );
};
