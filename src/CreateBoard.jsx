import "./CreateBoard.css";
import { useState } from "react";
import { generateBoard } from "./Api";
import Alert from "@mui/material/Alert";
import { Container } from "@mui/material";
import Snackbar from "@mui/material/Snackbar";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

export const CreateBoard = (props) => {
  const [isBoardClicked, setIsBoardClicked] = useState(false);
  const [boardName, setBoardName] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const displayBoardInput = () => {
    isBoardClicked ? setIsBoardClicked(false) : setIsBoardClicked(true);
  };

  const getName = (event) => {
    setBoardName(event.target.value);
  };

  const createBoard = async () => {
    if (!boardName) return;
    let response = await generateBoard(boardName);

    if (response.error) {
      setLoading(false);
      setError(response.error);
    } else {
      props.addBoard(response.data);
      setLoading(false);
      setError(null);
    }
  };

  return (
    <>
      <div className="create-board">
        {
          <form>
            <TextField sx={{backgroundColor:"white"}} onChange={getName} label="Type board Name" variant="outlined" />
            
            <Button onClick={createBoard} sx={{my:0.5}} variant="contained">Create</Button>

            <Container sx={{ my: 1 }}>
              {error && (
                <Snackbar open={true}>
                  <Alert severity="error">{error}</Alert>
                </Snackbar>
              )}
            </Container>
          </form>
        }
      </div>
    </>
  );
};
