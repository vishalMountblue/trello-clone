import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import IconButton from "@mui/material/IconButton";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import Grid from "@mui/material/Grid";
import { ListCard } from "./ListCard";
import { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import Button from "@mui/material/Button";
import { CreateCard } from "./CreateCard";
import { Archive } from "./Archive";
import { getAllCards } from "./Api";

export const Lists = (prop) => {
  const [cardDetails, setCardDetails] = useState([]);
  const [isCollapsed, setIsCollapsed] = useState(true);
  const [isPopoverCollapsed, setIsPopoverCollapsed] = useState(false);
  const [popoverAnchor, setPopoverAnchor] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  let list = prop.listData;
  let listArchiveFunction = prop.archiveFunction;

  useEffect(() => {
    const fetchAllCards = async () => {
      let response = await getAllCards(list);

      if (response.error) {
        setLoading(false);
        setError(response.error);
      } else {
        setCardDetails(response.data);
        setLoading(false);
        setError(null);
      }
    };

    fetchAllCards();
  }, []);

  const updateCardList = (newlyCreatedCard) => {
    setCardDetails([...cardDetails, newlyCreatedCard]);
  };

  const removeCard = (removedCardId) => {
    let allCards = cardDetails.filter((card) => {
      if (card.id === removedCardId) return null;
      return card;
    });

    setCardDetails(allCards);
  };

  const updateCollapseValue = () => {
    isCollapsed ? setIsCollapsed(false) : setIsCollapsed(true);
  };

  const updatePopoverCollapseValue = (event) => {
    isPopoverCollapsed
      ? setIsPopoverCollapsed(false)
      : setIsPopoverCollapsed(true);
    setPopoverAnchor(event.currentTarget);
  };

  if (error) {
    return <h1>{error} </h1>;
  }

  return (
    <>
      <List
        sx={{
          minWidth: 300,
          height: "fit-content",
          bgcolor: "gray",
          mx: 3,
          px: 1,
          borderRadius: 2,
        }}
      >
        <Grid container justifyContent={"space-between"}>
          <Grid item>
            <ListItemText primary={list.name} />
          </Grid>

          <Grid item>
            <IconButton onClick={updatePopoverCollapseValue} aria-label="more">
              <MoreHorizIcon />
              <Archive
                collapse={isPopoverCollapsed}
                anchor={popoverAnchor}
                listId={list.id}
                archiveList={listArchiveFunction}
              />
            </IconButton>
          </Grid>
        </Grid>

        {cardDetails.map((card) => {
          return (
            <ListItem key={card.id} sx={{ px: 0 }}>
              <ListCard key={card.id} data={card} scrapCard={removeCard} />
            </ListItem>
          );
        })}

        {isCollapsed ? (
          <Card onClick={updateCollapseValue} sx={{ width: "100%", my: 2 }}>
            <CardActions>
              <Button size="small">Add Card</Button>
            </CardActions>
          </Card>
        ) : (
          <CreateCard
            updateCard={updateCardList}
            collapse={updateCollapseValue}
            listId={list.id}
          />
        )}
      </List>
    </>
  );
};
