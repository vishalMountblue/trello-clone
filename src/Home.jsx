import { Header } from "./Header";
import { Board } from "./Board";
import { useState, useEffect } from "react";
import "./Home.css";
import { CreateBoard } from "./CreateBoard";
import { Link } from "react-router-dom";
import { fetchBoards } from "./Api";
import CircularProgress from "@mui/material/CircularProgress";

export const Home = () => {
  const [boardData, setBoardData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const addNewBoard = (boardObject) => {
    setBoardData([...boardData, boardObject]);
  };

  useEffect(() => {
    const getAllBoards = async () => {
      let response = await fetchBoards();

      if (response.error) {
        setLoading(false);
        setError(response.error);
      } else {
        setBoardData(response.data);
        setLoading(false);
        setError(null);
      }
    };
    getAllBoards();
  }, []);

  if (error) {
    return <h1>{error} </h1>;
  }

  return (
    <>
      <Link to="/boards">
        <Header />
        {loading ? (
          <CircularProgress
            sx={{ position: "absolute", right: "50%", top: "50%" }}
          />
        ) : (
          <main>
            <div className="work-space-wrapper">
              <label className="workspace-type">Your Workspace</label>
              <div className="work-space-container">
                {boardData.map((board) => {
                  return <Board key={board.id} boardData={board} />;
                })}

                <CreateBoard addBoard={addNewBoard} />
              </div>
            </div>
          </main>
        )}
      </Link>
    </>
  );
};
