import Popover from "@mui/material/Popover";
import Divider from "@mui/material/Divider";
import TextField from "@mui/material/TextField";
import { Container, IconButton } from "@mui/material";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { generateCheckList } from "./Api";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

export const CreateChecklist = (props) => {
  let card = props.cardData;

  const [checkListName, setCheckListName] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const getCheckListName = (event) => {
    setCheckListName(event.target.value);
  };

  const createCheckList = async () => {
    let response = await generateCheckList(card, checkListName);

    if (response.error) {
      setLoading(false);
      setError(response.error);
    } else {
      props.insertNewChecklistItem(response.data);
      setLoading(false);
      setError(null);
    }
  };

  const closeCreateCheckListPopover = (event) => {
    props.closeState(event);
  };


  return (
    <>
      <Popover
        open={props.openState}
        anchorEl={props.anchor}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
      >
        <Container sx={{ p: 2 }}>
          <Container sx={{ display: "flex", justifyContent: "space-between" }}>
            <Typography>Add Checklist</Typography>
            <IconButton onClick={closeCreateCheckListPopover}>
              <CloseIcon />
            </IconButton>
          </Container>

          <Divider light />
          <Typography sx={{ my: 2 }}>Title</Typography>
          <TextField
            onChange={getCheckListName}
            sx={{ width: "100%" }}
            rows={4}
            placeholder="Enter Checklist title...."
          />
          <Button onClick={createCheckList} sx={{ my: 2 }} variant="contained">
            Add
          </Button>

          <Container sx={{ my: 1 }}>
            {error && (
              <Snackbar open={true}>
                <Alert severity="error">{error}</Alert>
              </Snackbar>
            )}
          </Container>
        </Container>
      </Popover>
    </>
  );
};
