import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import DeleteIcon from "@mui/icons-material/Delete";
import { useState } from "react";
import { IconButton } from "@mui/material";
import { ChecklistModal } from "./ChecklistModal";
import { deleteCard } from "./Api";

export const ListCard = (props) => {
  let card = props.data;

  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const discardCard = async (event) => {
    event.stopPropagation();

    let response = await deleteCard(card);

    if (response.error) {
      setLoading(false);
      setError(response.error);
    } else {
      if (response.status === 200) props.scrapCard(card.id);
      setLoading(false);
      setError(null);
    }
  };

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  if (error) {
    return <h1>{error} </h1>;
  }

  return (
    <Card
      sx={{
        width: "100%",
        height: "fit-content",
        fontSize: "1rem",
        cursor: "pointer",
      }}
    >
      <CardContent
        onClick={handleOpen}
        sx={{ display: "flex", justifyContent: "space-between" }}
      >
        <Typography variant="caption text" component="div">
          {card.name}
        </Typography>
        <IconButton onClick={discardCard}>
          <DeleteIcon sx={{ color: "red" }} />
        </IconButton>
      </CardContent>

      {open && (
        <ChecklistModal
          openState={open}
          closeModal={handleClose}
          cardData={card}
        />
      )}
    </Card>
  );
};
