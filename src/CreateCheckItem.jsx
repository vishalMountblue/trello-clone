import Grid from "@mui/material/Grid";
import { useState } from "react";

import Card from "@mui/material/Card";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import { generateCheckItem } from "./Api";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";
import { Container } from "@mui/material";

export const CreateCheckItem = (props) => {
  let checklist = props.checkListData;
  const [checkItem, setCheckItem] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const updateCollapse = () => {
    props.collapse();
  };

  const handleInputChange = (event) => {
    setCheckItem(event.target.value);
  };

  const createCheckItem = async () => {
    if (!checkItem) return;
    
    let response = await generateCheckItem(checklist, checkItem);

    if (response.error) {
      setLoading(false);
      setError(response.error);
    } else {
      props.updateCheckItemsFunction(response.data);
      setLoading(false);
      setError(null);
    }
  };

  if (error) {
    return <h1>{error} </h1>;
  }

  return (
    <>
      <Grid container sx={{ flexDirection: "column", width: "100%" }}>
        <Grid item>
          <Card sx={{ width: "100%" }}>
            <TextField
              sx={{ width: "100%" }}
              rows={4}
              placeholder="Add an item...."
              onChange={handleInputChange}
            />
          </Card>
        </Grid>

        <Grid item>
          <Button onClick={createCheckItem} variant="contained" size="small">
            Add
          </Button>
          <IconButton onClick={updateCollapse} aria-label="more">
            <CloseIcon />
          </IconButton>
        </Grid>
      </Grid>
    </>
  );
};
