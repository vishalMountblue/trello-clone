import "./Header.css";
import { Link } from "react-router-dom";
export const Header = () => {
  return (
    <>
      <header>
        <div className="mode">
          <Link to={'/boards'} >
            <img className="large-icon" src="/trello.png"></img>
          </Link>
        </div>
      </header>
    </>
  );
};
