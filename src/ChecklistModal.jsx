import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import { useEffect, useState } from "react";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import CloseIcon from "@mui/icons-material/Close";
import { Container, IconButton } from "@mui/material";
import Typography from "@mui/material/Typography";
import CircularProgress from "@mui/material/CircularProgress";
import { Checklist } from "./Checklist";
import { CreateChecklist } from "./CreateChecklist";
import { getAllCheckList } from "./Api";

export const ChecklistModal = (props) => {
  let card = props.cardData;

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 700,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const [checklists, setChecklists] = useState([]);
  const [isPopoverCollapsed, setIsPopoverCollapsed] = useState(false);
  const [popoverAnchor, setPopoverAnchor] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchAllCheckList = async () => {
      let response = await getAllCheckList(card);

      if (response.error) {
        setLoading(false);
        setError(response.error);
      } else {
        setChecklists(response.data);
        setLoading(false);
        setError(null);
      }
    };

    fetchAllCheckList();
  }, []);

  const addNewlyCreatedChecklist = (newlyCreatedCheckList) => {
    setChecklists([...checklists, newlyCreatedCheckList]);
  };

  const deleteCheckList = (id) => {
    let filteredChecklist = checklists.filter((checkList) => {
      if (checkList.id === id) return null;
      return checkList;
    });

    setChecklists(filteredChecklist);
  };

  const handleOpen = (event) => {
    setIsPopoverCollapsed(true);
    setPopoverAnchor(event.currentTarget);
  };
  const handleClose = (event) => {
    event.stopPropagation();
    setIsPopoverCollapsed(false);
  };

  if (error) {
    return <h1>{error} </h1>;
  }

  return (
    <>
      <Modal open={props.openState}>
        {loading ? (
          <CircularProgress
            sx={{ position: "absolute", right: "50%", top: "50%" }}
          />
        ) : (
          <Box sx={style}>
            <Box
              sx={{ display: "flex", justifyContent: "space-between", my: 2 }}
            >
              <Typography variant="h6" component="div">
                {card.name}
              </Typography>
              <IconButton onClick={props.closeModal}>
                <CloseIcon />
              </IconButton>
            </Box>

            <Box sx={{ display: "flex", justifyContent: "end", my: 2 }}>
              <Button
                onClick={handleOpen}
                variant="contained"
                sx={{ p: 2, mx: 2 }}
              >
                <CheckBoxIcon />
                Create Checklist
                {isPopoverCollapsed && (
                  <CreateChecklist
                    openState={isPopoverCollapsed}
                    closeState={handleClose}
                    anchor={popoverAnchor}
                    cardData={card}
                    insertNewChecklistItem={addNewlyCreatedChecklist}
                  />
                )}
              </Button>
            </Box>

            <Box>
              {checklists.map((checkList) => {
                return (
                  <Checklist
                    checklistData={checkList}
                    removeChecklistItem={deleteCheckList}
                    cardData={card}
                    key={checkList.id}
                  />
                );
              })}
            </Box>
          </Box>
        )}
      </Modal>
    </>
  );
};
