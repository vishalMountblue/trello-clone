import Popover from "@mui/material/Popover";
import Button from "@mui/material/Button";
import { useState } from "react";
import { archive } from "./Api";
export const Archive = (props) => {
  let listId = props.listId;
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const archiveList = async () => {
    let response = await archive(listId);

    if (response.error) {
      setLoading(false);
      setError(response.error);
    } else {
      props.archiveList(response.data);
      setLoading(false);
      setError(null);
    }
  };

  if (error) {
    return <h1>{error} </h1>;
  }

  return (
    <>
      <Popover
        open={props.collapse}
        anchorEl={props.anchor}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
      >
        <Button onClick={archiveList} size="small" sx={{ p: 2 }}>
          Archive The List
        </Button>       
      </Popover>
    </>
  );
};
