import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import { Button, Container, IconButton } from "@mui/material";
import { CheckItem } from "./CheckItem";
import DeleteIcon from "@mui/icons-material/Delete";
import Box from "@mui/material/Box";
import { CreateCheckItem } from "./CreateCheckItem";
import { useState } from "react";
import { deleteChecklist } from "./Api";

export const Checklist = (props) => {
  let checklist = props.checklistData;
  let cardData = props.cardData;
  const [checkItems, setCheckItems] = useState(checklist.checkItems);
  const [isCollapsed, setIsCollapsed] = useState(true);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const removeChecklist = async () => {
    let response = await deleteChecklist(checklist);

    if (response.error) {
      setLoading(false);
      setError(response.error);
    } else {
      if (response.status === 200) {
        props.removeChecklistItem(checklist.id);
      }
      setLoading(false);
      setError(null);
    }
  };

  const updateCollapseValue = () => {
    isCollapsed ? setIsCollapsed(false) : setIsCollapsed(true);
  };

  const updateCheckItems = (newlyCreatedCheckItem) => {
    setCheckItems([...checkItems, newlyCreatedCheckItem]);
  };

  const deleteCheckItem = (checkItemId) => {
    let filteredCheckItems = checkItems.filter((checkItem) => {
      if (checkItem.id === checkItemId) return null;
      return checkItem;
    });

    setCheckItems(filteredCheckItems);
  };

  const updateSingleCheckItem = (checkObject) => {
    let updateCheckItems = checkItems.map((checkItem) => {
      if (checkItem.id === checkObject.id) return checkObject;
      return checkItem;
    });

    setCheckItems(updateCheckItems);
  };

  if (error) {
    return <h1>{error} </h1>;
  }

  return (
    <>
      <Card sx={{ mt: 2, p: 1 }}>
        <Container sx={{ display: "flex", flexDirection: "column" }}>
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Typography sx={{ my: 1 }} variant="h5">
              {checklist.name}
            </Typography>
            <IconButton onClick={removeChecklist}>
              <DeleteIcon sx={{ color: "red" }} />
            </IconButton>
          </Box>

          {checkItems.map((checkItem) => {
            return (
              <CheckItem
                checkItemData={checkItem}
                checklistData={checklist}
                removeCheckItem={deleteCheckItem}
                modifyCheckItems={updateSingleCheckItem}
                card={cardData}
                key={checkItem.id}
              />
            );
          })}

          {isCollapsed ? (
            <Button
              onClick={updateCollapseValue}
              sx={{ width: "fit-content", my: 2 }}
              variant="contained"
            >
              Add
            </Button>
          ) : (
            <CreateCheckItem
              collapse={updateCollapseValue}
              checkListData={checklist}
              updateCheckItemsFunction={updateCheckItems}
            />
          )}
        </Container>
      </Card>
    </>
  );
};
