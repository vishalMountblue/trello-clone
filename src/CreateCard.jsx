import Grid from "@mui/material/Grid";
import { useState } from "react";
import Card from "@mui/material/Card";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import { generateCardInList } from "./Api";
import Alert from "@mui/material/Alert";
import { Container } from "@mui/material";
import Snackbar from '@mui/material/Snackbar';

export const CreateCard = (props) => {
  let collapse = props.collapse;
  let listId = props.listId;
  const [itemName, setItemName] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const handleInputChange = (event) => {
    setItemName(event.target.value);
  };

  const updateCollapse = () => {
    collapse();
  };

  const addCard = async () => {
    if (!itemName) return;
    let response = await generateCardInList(listId, itemName);

    if (response.error) {
      setLoading(false);
      setError(response.error);
    } else {
      props.updateCard(response.data);
      setLoading(false);
      setError(null);
    }
  };

  return (
    <>
      <Grid container sx={{ flexDirection: "column", width: "100%" }}>
        <Grid item>
          <Card sx={{ width: "100%" }}>
            <TextField
              sx={{ width: "100%" }}
              rows={4}
              placeholder="Enter your card name...."
              onChange={handleInputChange}
            />
          </Card>
        </Grid>

        <Grid item>
          <Button onClick={addCard} variant="contained" size="small">
            Add Card
          </Button>
          <IconButton onClick={updateCollapse} aria-label="more">
            <CloseIcon />
          </IconButton>
        </Grid>

        <Container sx={{ my: 1 }}>
          {error && (
            <Snackbar open={true} >
              <Alert severity="error">{error}</Alert>
            </Snackbar>
          )}
        </Container>
      </Grid>
    </>
  );
};
