import { Header } from "./Header";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Lists } from "./Lists";
import "./Workspace.css";
import { CreateList } from "./CreateList";
import { getAllLists } from "./Api";
import CircularProgress from "@mui/material/CircularProgress";

export const Workspace = () => {
  let board = useParams();
  const [lists, setLists] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchAllLists = async () => {
      let response = await getAllLists(board);
      
      if (response.error) {
        setLoading(false);
        setError(response.error);
      } else {
        setLists(response.data);
        setLoading(false);
        setError(null);
      }
    };

    fetchAllLists();
  }, []);

  const updateLists = (newlyCreatedList) => {
    setLists([...lists, newlyCreatedList]);
  };

  const archiveListItem = (archivedList) => {
    let list = lists.filter((eachListItem) => {
      if (eachListItem.id === archivedList.id) return null;
      return eachListItem;
    });

    setLists(list);
  };

  if (error) {
    return <h1>{error} </h1>;
  }

  return (
    <>
      <Header />

      {loading ? (
        <CircularProgress
          sx={{ position: "absolute", right: "50%", top: "50%" }}
        />
      ) : (
        <main>
          <div className="list-container">
            {lists.map((list) => {
              return (
                <Lists
                  key={list.id}
                  listData={list}
                  archiveFunction={archiveListItem}
                />
              );
            })}

            <CreateList board={board.id} listUpdate={updateLists} />
          </div>
        </main>
      )}
    </>
  );
};
